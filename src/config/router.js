import { Home, Login, DetailPage, CheckoutPage, SuccessPage } from "../screens";

const router = {
  Home: {
    path: "/",
    component: Home,
    isPrivate: false,
  },
  Login: {
    path: "/login",
    component: Login,
    isPrivate: false,
  },
  Api: {
    Login: "/login",
    Signup: "/users/register",
    Me: "/me",
    GetService: "/service",
    Subscription: "/subscription",
  },
  DetailPage: {
    path: "/pricing/:id",
    component: DetailPage,
    isPrivate: false,
  },
  CheckoutPage: {
    path: "/checkout",
    component: CheckoutPage,
    isPrivate: false,
  },
  SuccessPage: {
    path: "/success",
    component: SuccessPage,
    isPrivate: false,
  },
};

export default router;
