import ThemeProvider from "./Providers/ThemeProvider";
import AuthProvider from "./Providers/AuthProvider";
import AlertProvider from "./Providers/AlertProvider";
import RouterApp from "./router";

const App = () => (
  <ThemeProvider>
    <AlertProvider>
      <AuthProvider>
        <RouterApp />
      </AuthProvider>
    </AlertProvider>
  </ThemeProvider>
);

export default App;
