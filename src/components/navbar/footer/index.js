import React from "react";
import { AppBar, Typography, Avatar, Grid, Hidden } from "@mui/material";
import { useTheme } from "../../../Providers/ThemeProvider";

const Footer = () => {
  const { theme } = useTheme();
  return (
    <AppBar
      position="static"
      sx={{
        border: theme.palette.border.borderOne,
        boxShadow: "none",
        minHeight: "16vh",
        padding: 4,
      }}
    >
      <Grid container direction="row" justifyContent="center" alignItems="center">
        <Hidden smDown>
          <Grid item xs={4} md={3}>
            <Typography
              variant="h6"
              component="div"
              sx={{
                flexGrow: 1,
                color: theme.palette.text.primary,
                fontSize: 14,
                fontWeight: 600,
              }}
            >
              <Avatar
                sx={{
                  borderRadius: 2,
                  border: theme.palette.border.borderOne,
                  color: theme.palette.text.primary,
                }}
                src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTIBeGwc_hxfUV5WLGoXIj9Q6OLnraLPid2k7si6nUgczVW23qc"
              />
              © 2022-2030
            </Typography>
          </Grid>
        </Hidden>
        <Grid
          item
          xs={4}
          md={3}
          sx={{
            textAlign: "center",
            color: theme.palette.text.primary,
          }}
        >
          Features
        </Grid>
        <Grid
          item
          xs={4}
          md={3}
          sx={{
            color: theme.palette.text.primary,
            textAlign: "center",
          }}
        >
          Resources
        </Grid>
        <Grid
          item
          xs={4}
          md={3}
          sx={{
            color: theme.palette.text.primary,
            textAlign: "center",
          }}
        >
          About
        </Grid>
      </Grid>
    </AppBar>
  );
};
export default Footer;
