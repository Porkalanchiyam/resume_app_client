import React from "react";
import { AppBar, Toolbar, Typography, Avatar, Hidden } from "@mui/material";
import DarkModeIcon from "@mui/icons-material/DarkMode";
import ButtonMui from "../../Button";
import { useTheme } from "../../../Providers/ThemeProvider";

const TopBar = () => {
  const { theme, changeTheme } = useTheme();
  return (
    <div>
      <AppBar
        position="static"
        sx={{
          borderBottom: theme.palette.border.borderOne,
          boxShadow: "none",
        }}
      >
        <Toolbar>
          <Hidden smDown>
            <Typography
              variant="h6"
              component="div"
              sx={{
                flexGrow: 1,
                color: theme.palette.text.primary,
              }}
            >
              Welcome to 🔥
            </Typography>
          </Hidden>

          <Avatar
            sx={{
              borderRadius: 2,
              border: theme.palette.border.borderOne,
            }}
            src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTIBeGwc_hxfUV5WLGoXIj9Q6OLnraLPid2k7si6nUgczVW23qc"
          />
          <ButtonMui
            sx={{
              boxShadow: theme.palette.boxShadow.shadowOne,
              color: theme.palette.text.primary,
              margin: "0px 8px",
              borderRadius: 2,
              width: "20px",
              border: theme.palette.border.borderOne,
            }}
            onClick={() => {
              changeTheme();
            }}
          >
            <DarkModeIcon sx={{ fontSize: 26 }} />
          </ButtonMui>

          <ButtonMui
            color="primary"
            sx={{
              boxShadow: theme.palette.boxShadow.shadowOne,
              color: theme.palette.text.primary,
              border: theme.palette.border.borderOne,
              fontWeight: 600,
            }}
          >
            Log-Out
          </ButtonMui>
        </Toolbar>
      </AppBar>
    </div>
  );
};
export default TopBar;
