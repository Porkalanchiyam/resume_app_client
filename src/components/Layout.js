import React, { useEffect } from "react";
import CircularProgress from "@mui/material/CircularProgress";
import Backdrop from "@mui/material/Backdrop";
import Typography from "@mui/material/Typography";
import Grid from "@mui/material/Grid";
import { useLocation, useNavigate } from "react-router-dom";
import { useTheme } from "../Providers/ThemeProvider";

import routes from "../config/router";
import { useAuth } from "../Providers/AuthProvider";
import TopBar from "./navbar/topbar";
import Footer from "./navbar/footer";

const Layout = ({ isCenter = false, isPrivate, loading, showChildOnLoading, loadingText = "Loading", children }) => {
  const navigate = useNavigate();
  const location = useLocation();
  const { user } = useAuth();
  const { theme } = useTheme();

  useEffect(() => {
    if (isPrivate) {
      if (!user.loading && !user.loggedIn) {
        navigate(`${routes.Login.path}?redirect=${location.pathname}`);
      }
    }
  }, [user]);

  if (loading) {
    return (
      <>
        <Backdrop
          sx={{
            color: "#fff",
            display: "flex",
            flexDirection: "column",
          }}
          open={loading}
        >
          <CircularProgress color="inherit" />
          <Typography sx={{ mt: 3 }} variant="h6" gutterBottom component="div">
            {loadingText}
          </Typography>
        </Backdrop>
        {showChildOnLoading && (
          <Grid container>
            <Grid item xs={12}>
              <TopBar />
            </Grid>
            <Grid
              item
              xs={12}
              sx={{
                background: theme.palette.primary.dark,
                paddingBottom: 4,
              }}
            >
              {children}
            </Grid>
            <Grid item xs={12}>
              <Footer />
            </Grid>
          </Grid>
        )}
      </>
    );
  }
  if (isCenter) {
    return (
      <Grid container>
        <Grid item xs={12}>
          <TopBar />
        </Grid>
        <Grid
          item
          xs={12}
          sx={{
            background: theme.palette.primary.dark,
            paddingBottom: 4,
          }}
        >
          {children}
        </Grid>
        <Grid item xs={12}>
          <Footer />
        </Grid>
      </Grid>
    );
  }
  return (
    <Grid container>
      <Grid item xs={12}>
        <TopBar />
      </Grid>
      <Grid
        item
        xs={12}
        sx={{
          background: theme.palette.primary.dark,
          paddingBottom: 4,
        }}
      >
        {children}
      </Grid>
      <Grid item xs={12}>
        <Footer />
      </Grid>
    </Grid>
  );
};

export default Layout;
