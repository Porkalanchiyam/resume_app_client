import { Typography, List, ListItem, Divider, ListItemText, Card, CardHeader } from "@mui/material";
import { useEffect } from "react";
import { useNavigate } from "react-router-dom";
import router from "../config/router";

import { useTheme } from "../Providers/ThemeProvider";

const CartCard = (props) => {
  const { cardDetails } = props;
  const { theme } = useTheme();
  const navigate = useNavigate();

  useEffect(() => {
    if (!cardDetails) {
      navigate(router.Home.path);
    }
  }, [cardDetails]);

  return (
    <Card
      sx={{
        background: theme.palette.primary.main,
        borderRadius: 4,
        border: "1px solid #00000029 ",
      }}
      elevation={0}
    >
      <CardHeader
        title={
          // eslint-disable-next-line react/jsx-wrap-multilines
          <Typography sx={{ fontWeight: "bold" }} variant="h5">
            Your Cart
          </Typography>
        }
      />
      <List sx={{ width: "100%" }}>
        <ListItem
          secondaryAction={
            // eslint-disable-next-line react/jsx-wrap-multilines
            <Typography sx={{ display: "inline" }} component="span" variant="body2" color="text.primary">
              {`$${cardDetails.price.amount}`}
            </Typography>
          }
          alignItems="flex-start"
        >
          <ListItemText primary={cardDetails.selectedService} secondary={cardDetails.name} />
        </ListItem>

        <Divider />

        <ListItem
          secondaryAction={
            // eslint-disable-next-line react/jsx-wrap-multilines
            <Typography sx={{ display: "inline" }} component="h6" variant="h6" color="text.primary">
              {`$${cardDetails.price.amount}`}
            </Typography>
          }
          alignItems="flex-start"
        >
          <ListItemText primary="Total (USD)" />
        </ListItem>
      </List>
    </Card>
  );
};
export default CartCard;
