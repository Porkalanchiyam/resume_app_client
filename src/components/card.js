import * as React from "react";
import { useNavigate } from "react-router-dom";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Typography from "@mui/material/Typography";
import { CardActionArea, CardActions } from "@mui/material";
import { useTheme } from "../Providers/ThemeProvider";
import Button from "./Button";

export default function MultiActionAreaCard(props) {
  const navigate = useNavigate();
  const { name, description, _id } = props;
  const { theme } = useTheme();

  const handleViewDetais = () => {
    navigate(`/pricing/${_id}`);
  };
  return (
    <Card
      sx={{
        background: theme.palette.primary.main,
        boxShadow: "0 3px 6px 0 #00000029",
        margin: 2,
        borderRadius: 4,
        textAlign: "center",
      }}
    >
      <CardActionArea>
        <CardContent
          sx={{
            minHeight: "310px",
            textAlign: "center",
          }}
        >
          <Typography variant="h6" sx={{ marginBottom: 2, fontSize: 24 }}>
            {name || ""}
          </Typography>

          <Typography variant="subtitle1">{description}</Typography>
        </CardContent>
      </CardActionArea>
      <CardActions
        sx={{
          display: "flex",
          justifyContent: "center",
        }}
      >
        <Button
          onClick={() => handleViewDetais()}
          sx={{
            boxShadow: theme.palette.boxShadow.shadowOne,
            color: theme.palette.text.primary,
            margin: "0px 8px",
            borderRadius: 2,
            border: theme.palette.border.borderOne,
          }}
          size="small"
        >
          View Details
        </Button>
      </CardActions>
    </Card>
  );
}
