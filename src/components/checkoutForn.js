import * as React from "react";
import Box from "@mui/material/Box";
import { FormControlLabel, Grid, Radio, RadioGroup, TextField, Typography } from "@mui/material";
import Autocomplete from "@mui/material/Autocomplete";
import Button from "./Button";
import { useTheme } from "../Providers/ThemeProvider";
import { validationMiddleware } from "../utils";
import { signUpSchema } from "../Schema/register";
import { AlertContext } from "../Providers/AlertProvider";

const country = [
  {
    label: "India",
    value: "IN",
    states: [{ label: "Tamil Nadu", value: "TN" }],
  },
  {
    label: "United States Of America",
    value: "USA",
    states: [{ label: "New York", value: "NY" }],
  },
];
export default function CheckoutForm(props) {
  const { theme, themeName } = useTheme();
  const { setAlertInfo } = React.useContext(AlertContext);
  const { handleCheckoutForm } = props;
  const [values, setValues] = React.useState({
    firstName: "",
    lastName: "",
    email: "",
    address: "",
    zip: "",
    country: {},
    state: {},
    payment: "paypal",
    error: {},
  });

  const handleChange = (event) => {
    setValues({
      ...values,
      [event.target.name]: event.target.value,
      error: {
        ...values.error,
        [event.target.name]: false,
      },
    });
  };

  const handleCheckout = async () => {
    const validation = await validationMiddleware(signUpSchema, values);

    if (validation.status) {
      handleCheckoutForm(values);
    } else {
      setValues({ ...values, error: validation.errors });
      setAlertInfo({
        msg: "Please fill all the required fields",
        severity: "error",
      });
    }
  };

  return (
    <Box
      sx={{
        padding: "20px",
        background: theme.palette.primary.main,
        borderRadius: 4,
        border: "1px solid #00000029 ",
        marginBottom: "20px",
      }}
    >
      <Grid container spacing={4}>
        <Grid item xs={12}>
          <Typography sx={{ fontWeight: "bold" }} variant="h5">
            Please Fill Checkout Form
          </Typography>
        </Grid>
        <Grid item md={6} xs={12}>
          <TextField
            fullWidth
            autoComplete="off"
            size="small"
            id="first_name"
            name="firstName"
            onChange={(e) => handleChange(e)}
            error={values?.error?.firstName}
            value={values.firstName}
            label="First Name"
            color="secondary"
            variant="outlined"
            helperText={values?.error?.firstName && "Please Enter Your First Name"}
            className={themeName !== "light" && "inputcls"}
            required
          />
        </Grid>
        <Grid item md={6} xs={12}>
          <TextField
            fullWidth
            autoComplete="off"
            size="small"
            id="last_name"
            name="lastName"
            onChange={(e) => handleChange(e)}
            error={values?.error?.lastName}
            value={values.lastName}
            label="Last Name"
            color="secondary"
            variant="outlined"
            helperText={values?.error?.lastName && "Please Enter Your Last Name"}
            className={themeName !== "light" && "inputcls"}
            required
          />
        </Grid>
        <Grid item md={12} xs={12}>
          <TextField
            fullWidth
            autoComplete="off"
            size="small"
            id="email"
            name="email"
            error={values?.error?.email}
            onChange={(e) => handleChange(e)}
            value={values.email}
            label="Email"
            color="secondary"
            className={themeName !== "light" && "inputcls"}
            variant="outlined"
            helperText={values?.error?.email && "Please Enter Valid Email"}
            required
          />
        </Grid>

        <Grid item md={12} xs={12}>
          <TextField
            fullWidth
            autoComplete="off"
            size="small"
            id="Address"
            onChange={(e) => handleChange(e)}
            name="address"
            error={values?.error?.address}
            value={values.address}
            label="Address"
            color="secondary"
            variant="outlined"
            className={themeName !== "light" && "inputcls"}
            helperText={values?.error?.address && "Please Enter Your Address"}
            required
          />
        </Grid>
        <Grid item md={4} xs={12}>
          <Autocomplete
            disablePortal
            disableClearable
            id="combo-box-demo"
            options={country}
            value={values.country?.value ? values.country : null}
            onChange={(e, value) => {
              setValues({
                ...values,
                country: value,
                error: {
                  ...values.error,
                  country: false,
                },
              });
            }}
            size="small"
            fullWidth
            renderInput={(params) => (
              <TextField
                className={themeName !== "light" && "inputcls"}
                error={values.error?.country}
                helperText={values.error?.country && "Please select your country"}
                required
                color="secondary"
                {...params}
                name="country"
                label="Country"
              />
            )}
          />
        </Grid>
        <Grid item md={4} xs={12}>
          <Autocomplete
            disablePortal
            disableClearable
            id="combo-box-demo"
            options={values?.country?.states ?? []}
            value={values.state?.value ? values.state : null}
            onChange={(e, value) => {
              setValues({
                ...values,
                state: value,
                error: {
                  ...values.error,
                  state: false,
                },
              });
            }}
            size="small"
            fullWidth
            isOptionEqualToValue={(option, value) => option.value === value.value}
            renderInput={(params) => (
              <TextField
                className={themeName !== "light" && "inputcls"}
                error={values.error?.state}
                helperText={values.error?.state && "Please select your state"}
                required
                color="secondary"
                {...params}
                name="state"
                label="State"
              />
            )}
          />
        </Grid>
        <Grid item md={4} xs={12}>
          <TextField
            fullWidth
            autoComplete="off"
            size="small"
            id="Zip"
            onChange={(e) => handleChange(e)}
            name="zip"
            error={values?.error?.zip}
            value={values.zip}
            label="Zip"
            color="secondary"
            variant="outlined"
            helperText={values?.error?.zip && "Please Enter Your Zip"}
            required
            className={themeName !== "light" && "inputcls"}
          />
        </Grid>
        <Grid item xs={4}>
          <Typography sx={{ fontWeight: "bold" }} variant="h5">
            Payment Method
          </Typography>
          <RadioGroup row name="payment" onChange={(e) => handleChange(e)} value={values?.payment} defaultValue="paypal">
            <FormControlLabel color="secondary" value="paypal" control={<Radio color="secondary" />} label="Pay Pal" />
          </RadioGroup>
        </Grid>
        <Grid item sm={12}>
          <Button
            onClick={() => handleCheckout()}
            sx={{
              boxShadow: theme.palette.boxShadow.shadowOne,
              color: theme.palette.text.primary,
              margin: "0px 8px",
              borderRadius: 2,
              border: theme.palette.border.borderOne,
              width: "100%",
            }}
            size="small"
          >
            Continue to checkout
          </Button>
        </Grid>
      </Grid>
    </Box>
  );
}
