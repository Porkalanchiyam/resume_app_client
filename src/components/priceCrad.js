import { Card, CardContent, Typography, CardActions, CardHeader } from "@mui/material";
import { useNavigate } from "react-router-dom";
import router from "../config/router";
import { useTheme } from "../Providers/ThemeProvider";
import Button from "./Button";

const PriceCard = (props) => {
  const navigate = useNavigate();
  const { theme } = useTheme();
  const { item, selectedService, selectedServiceID } = props;
  return (
    <Card
      sx={{
        background: theme.palette.primary.main,
        boxShadow: "0 3px 6px 0 #00000029",
        margin: 2,
        borderRadius: 4,
        textAlign: "center",
      }}
    >
      <CardHeader
        sx={{
          borderBottom: theme.palette.border.borderOne,
        }}
        title={<Typography variant="h6">{item?.name ?? ""}</Typography>}
      />
      <CardContent>
        <Typography variant="h4" component="div">
          {item?.price?.symbol ?? ""}
          {item?.price?.amount ?? ""}
          <span>/</span>
          <span style={{ fontSize: 22, fontWeight: 100 }}>{item?.price?.term ?? ""}</span>
        </Typography>
        {item?.features?.map((child) => (
          <Typography key={child} variant="subtitle1">
            {child ?? ""}
          </Typography>
        ))}
      </CardContent>
      <CardActions sx={{ justifyContent: "center" }}>
        <Button
          onClick={() => {
            navigate(router.CheckoutPage.path, {
              state: { ...item, selectedService, selectedServiceID },
            });
          }}
          sx={{
            boxShadow: theme.palette.boxShadow.shadowOne,
            color: theme.palette.text.primary,
            margin: "0px 8px",
            borderRadius: 2,
            border: theme.palette.border.borderOne,
          }}
          size="small"
        >
          {item?.actionName ?? ""}
        </Button>
      </CardActions>
    </Card>
  );
};
export default PriceCard;
