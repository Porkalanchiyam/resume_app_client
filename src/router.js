import React from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";

import routes from "./config/router";
import NotFound from "./screens/404";

const RouterApp = () => (
  <BrowserRouter>
    <Routes>
      {Object.keys(routes).map((key) => {
        if (key === "Api") {
          return null;
        }
        const {
          component: { default: Component },
        } = routes[key];
        return (
          <Route
            key={key}
            path={routes[key].path}
            // eslint-disable-next-line react/jsx-pascal-case
            element={<Component isPrivate={routes[key].isPrivate} />}
          />
        );
      })}
      {/* For unknow/non-defined path */}
      <Route path="*" element={<NotFound />} />
    </Routes>
  </BrowserRouter>
);

export default RouterApp;
