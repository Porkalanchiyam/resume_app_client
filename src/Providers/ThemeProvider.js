import React, { useContext, useEffect, useState } from "react";
import { ThemeProvider as MuiThemeProvider } from "@mui/material/styles";
import { StyledEngineProvider } from "@mui/material";
import CssBaseline from "@mui/material/CssBaseline";

import theme from "../lib/theme";

export const ThemeContext = React.createContext({
  themeName: "light",
  theme: theme.light,
  changeTheme: () => {},
});

const ThemeProvider = ({ children }) => {
  const [themeName, setTheme] = useState("dark");

  useEffect(() => {
    // set theme based on local storage
    const localTheme = localStorage.getItem("theme");
    if (localTheme) {
      setTheme(localTheme);
    }
  }, []);

  const changeTheme = () => {
    const newTheme = themeName === "light" ? "dark" : "light";
    setTheme(newTheme);
    localStorage.setItem("theme", newTheme);
  };

  return (
    <ThemeContext.Provider
      value={{
        theme: theme[themeName],
        themeName,
        changeTheme,
      }}
    >
      <StyledEngineProvider injectFirst>
        <MuiThemeProvider theme={theme[themeName]}>
          <CssBaseline />
          {children}
        </MuiThemeProvider>
      </StyledEngineProvider>
    </ThemeContext.Provider>
  );
};

export const useTheme = () => useContext(ThemeContext);

export default ThemeProvider;
