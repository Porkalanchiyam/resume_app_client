import React, { useState } from "react";

import Alerts from "../components/Alert";

export const AlertContext = React.createContext({
  setAlertInfo: () => {},
});

const AlertProvider = ({ children }) => {
  const [alertInfo, setAlertInfo] = useState({
    horizontal: "left",
    msg: "",
    open: false,
    severity: "success",
    vertical: "bottom",
  });

  const onclose = () => {
    setAlertInfo({
      horizontal: "left",
      msg: "",
      open: false,
      severity: "success",
      vertical: "bottom",
    });
  };

  return (
    <AlertContext.Provider
      value={{
        setAlertInfo: (val) => {
          setAlertInfo({
            open: val?.open ?? true,
            horizontal: val?.horizontal || "right",
            vertical: val?.vertical || "top",
            severity: val?.severity || "success",
            msg: val.msg,
          });
        },
      }}
    >
      {alertInfo.open ? <Alerts {...alertInfo} onclose={onclose} /> : ""}
      {children}
    </AlertContext.Provider>
  );
};

export const useAlert = () => React.useContext(AlertContext);

export default AlertProvider;
