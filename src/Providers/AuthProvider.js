import React, {
  useState,
  useEffect,
  useContext,
  createContext,
} from "react";
import Backdrop from "@mui/material/Backdrop";
import CircularProgress from "@mui/material/CircularProgress";
import Typography from "@mui/material/Typography";

import routes from "../config/router";
import { getDefaultProfileImg, fetcher } from "../utils";
import { AlertContext } from "./AlertProvider";

const authContext = createContext({
  user: {
    loggedIn: false,
    email: "",
    emailVerified: false,
    id: "",
    firstName: "",
    lastName: "",
    phone: "",
    image: "",
    createdAt: new Date(),
    updatedAt: new Date(),
    loading: true,
  },
  setUser: () => {},
});

const useProvideAuth = () => {
  const [user, setUser] = useState({
    loggedIn: false,
    email: "",
    emailVerified: false,
    id: "",
    firstName: "",
    lastName: "",
    image: "",
    phone: "",
    createdAt: new Date(),
    updatedAt: new Date(),
    loading: true,
  });
  const { setAlertInfo } = useContext(AlertContext);

  const fetchUserInfo = async () => {
    const response = await fetcher(
      routes.Api.Me,
      "POST",
      {},
    );
    if (response.status === "SUCCESS") {
      const {
        user: fetchedUser,
      } = response.data;
      if (!fetchedUser.image) {
        fetchedUser.image = getDefaultProfileImg(`${fetchedUser.firstName} ${fetchedUser.lastName}`);
      }
      setUser({
        ...fetchedUser,
        loggedIn: true,
        loading: false,
      });
    } else if (response.data === "Network Called Failed") {
      setAlertInfo({
        msg: response.data,
        severity: "error",
      });
    } else {
      localStorage.removeItem(process.env.tokenKey || "");
    }
  };

  useEffect(() => {
    const token = localStorage.getItem(process.env.tokenKey || "");
    if (token) {
      fetchUserInfo()
        .catch(() => {
          setUser({
            ...user,
            loading: false,
          });
        });
    } else {
      setUser({
        ...user,
        loading: false,
      });
    }
  }, []);

  return {
    user,
    setUser,
  };
};

const AuthProvider = ({ children }) => {
  const auth = useProvideAuth();
  return (
    <authContext.Provider value={auth}>
      {
        auth.user.loading
          ? (
            <Backdrop
              sx={{
                color: "#fff",
                zIndex: (theme) => theme.zIndex.drawer + 1,
                display: "flex",
                flexDirection: "column",
              }}
              open
            >
              <CircularProgress color="inherit" />
              <Typography sx={{ mt: 3 }} variant="h6" gutterBottom component="div">
                LOADING
              </Typography>
            </Backdrop>
          ) : children
      }
    </authContext.Provider>
  );
};

export const useAuth = () => useContext(authContext);

export default AuthProvider;
