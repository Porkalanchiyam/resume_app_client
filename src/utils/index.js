import { format } from "date-fns";

export const addTime = (minutes) => new Date(Date.now() + minutes * 60000);

export const getDefaultProfileImg = (username) => `https://avatars.dicebear.com/api/initials/${encodeURIComponent(username)}.svg`;

export const sleep = (t) => new Promise((r) => setTimeout(r, t));

export const formatDate = (date = new Date(), formatVal = "yyyy-MM-dd") => format(date, formatVal);
export const validationMiddleware = async (schema, body) => {
  try {
    await schema.validate(body, { abortEarly: false });
    return {
      status: true,
      errors: {},
    };
  } catch (err) {
    // Handle schema validation error
    // eslint-disable-next-line no-console
    console.log(err);
    const errors = {};
    err.inner.forEach((e) => {
      errors[e.path.split(".")[0]] = true;
    });
    return {
      status: false,
      errors,
    };
  }
};

export * from "./fetcher";
