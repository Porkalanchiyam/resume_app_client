const makeUrl = (url) => `${process.env.REACT_APP_API_URL}${url}`;

const fetcher = async (url, method, input) => {
  try {
    const headers = new Headers({
      "Content-Type": "application/json",
    });
    const token = localStorage.getItem(process.env.tokenKey || "");
    if (token) {
      headers.append("authorization", `Bearer ${token}`);
    }
    const options = {
      method: method.toUpperCase(),
      headers,
      credentials: "same-origin",
    };
    if (method === "POST") {
      if (input) {
        options.body = JSON.stringify(input);
      } else {
        options.body = JSON.stringify({});
      }
    }
    const res = await fetch(makeUrl(url), options);

    return await res.json();
  } catch (err) {
    return {
      status: "FAILED",
      message: "Network Called Failed",
    };
  }
};

const MakeRequest = async (url, method, input) => {
  try {
    const response = await fetcher(url, method, input);
    if (response.status === "SUCCESS") {
      return {
        data: response.data,
        success: true,
      };
    }
    return {
      data: response.message,
      success: false,
    };
  } catch (e) {
    return {
      data: e,
      success: false,
    };
  }
};

export { fetcher, MakeRequest };
