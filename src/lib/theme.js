import { createTheme } from "@mui/material/styles";

const lightTheme = createTheme({
  palette: {
    common: {
      black: "#000",
      white: "#fff",
    },
    background: {
      paper: "#fff",
      default: "#fafafa",
    },
    primary: {
      light: "#2058f1",
      main: "#ffffff",
      dark: "linear-gradient(to bottom, #f0f0f0, #ffffff);",
      contrastText: "#fff",
    },
    secondary: {
      light: "rgba(42, 46, 82, 0.1);",
      main: "rgb(54, 58, 101)",
      dark: "#0b0d1afa",
      contrastText: "#fff",
    },
    error: {
      light: "#e57373",
      main: "#f44336",
      dark: "#d32f2f",
      contrastText: "#fff",
    },
    text: {
      primary: "rgba(16, 14, 29, 0.8)",
      secondary: "rgba(16, 14, 29, 0.17)",
      disabled: "rgba(0, 0, 0, 0.38)",
      hint: "rgba(0, 0, 0, 0.38)",
      hover: "#85878a",
    },
    boxShadow: {
      shadowOne: "0 3px 6px 0 #00000012",
    },
    border: {
      borderOne: "2px solid #67637d36",
    },
  },
});

const darkTheme = createTheme({
  palette: {
    common: {
      black: "#000",
      white: "#fff",
    },
    background: {
      paper: "#100e1d",
      default: "#fafafa",
    },
    primary: {
      light: "#2058f1",
      main: "#2a2e52",
      dark: "#1e213a",
      contrastText: "#fff",
    },
    secondary: {
      light: "#ffffff",
      main: "rgb(54, 58, 101)",
      dark: "#0b0d1afa",
      contrastText: "#fff",
    },
    error: {
      light: "#e57373",
      main: "#f44336",
      dark: "#d32f2f",
      contrastText: "#fff",
    },
    text: {
      primary: "rgba(255, 255, 255, 0.8)",
      secondary: "rgba(108, 110, 131, 0.3)",
      disabled: "rgba(0, 0, 0, 0.38)",
      hint: "rgba(0, 0, 0, 0.38)",
      hover: "rgb(32, 88, 241)",
    },
    boxShadow: {
      shadowOne: "0 3px 6px 0 #00000012",
    },
    border: {
      borderOne: "2px solid #67637d36",
      Inputborder: "2px solid #686285eb",
    },
  },
});

const theme = {
  light: lightTheme,
  dark: darkTheme,
};

export default theme;
