import { Grid, Typography } from "@mui/material";
import React, { useContext } from "react";
import { useParams } from "react-router-dom";
import Layout from "../../components/Layout";
import PriceCard from "../../components/priceCrad";
import router from "../../config/router";
import { AlertContext } from "../../Providers/AlertProvider";
import { MakeRequest } from "../../utils";

const DetailPage = ({ isPrivate }) => {
  const [state, setState] = React.useState([]);
  const { setAlertInfo } = useContext(AlertContext);
  const location = useParams();

  React.useEffect(() => {
    MakeRequest(`${router.Api.GetService}/${location.id}`, "GET").then((res) => {
      if (res.success) {
        setState(res.data);
      } else {
        setAlertInfo(res.data, "error");
      }
    });
  }, []);

  return (
    <Layout isCenter isPrivate={isPrivate}>
      <Typography
        variant="h4"
        sx={{
          paddingTop: 6,
          textAlign: "center",
        }}
      >
        {state?.name ?? ""}
      </Typography>
      <Typography
        variant="subtitle1"
        sx={{
          fontSize: 24,
          textAlign: "center",
          padding: 8,
        }}
      >
        {state?.description ?? ""}
      </Typography>
      <Grid container justifyContent="center">
        {state?.tiers?.map((item) => (
          <Grid item xs={3} key={item?._id}>
            <PriceCard
              selectedService={state?.name}
              selectedServiceID={state?._id}
              item={item}
              key={item?._id}
            />
          </Grid>
        ))}
      </Grid>
    </Layout>
  );
};
export default DetailPage;
