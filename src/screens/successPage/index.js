/* eslint-disable no-param-reassign */
import { Card, Container, Grid, Typography } from "@mui/material";
import React, { useContext } from "react";
import { useLocation, useNavigate } from "react-router-dom";
import Button from "../../components/Button";
import Layout from "../../components/Layout";
import { AlertContext } from "../../Providers/AlertProvider";
import { useTheme } from "../../Providers/ThemeProvider";

const SuccessPage = ({ isPrivate }) => {
  const { setAlertInfo } = useContext(AlertContext);
  const navigate = useNavigate();
  const { theme } = useTheme();
  const { state } = useLocation();

  const handleBackHome = (data) => {
    navigate("/");
  };

  return (
    <Layout isCenter isPrivate={isPrivate}>
      <Card
        sx={{
          background: theme.palette.primary.main,
          borderRadius: 1,
          border: "1px solid #00000029 ",
          minHeight: "70vh",
          width: "60%",
          margin: "auto",
        }}
        elevation={0}
      >
        <Typography
          variant="h4"
          sx={{
            paddingTop: 6,
            textAlign: "center",
          }}
        >
          Thankyou for Subscribing!
        </Typography>
        <Typography
          variant="subtitle1"
          sx={{
            fontSize: 24,
            textAlign: "center",
            padding: 8,
          }}
        >
          {`You have Successfully Subscribed ${state?.serviceName} Tier for ${state?.tierName} . Enjoy Services.`}
        </Typography>
        <Button
          onClick={() => handleBackHome()}
          sx={{
            boxShadow: theme.palette.boxShadow.shadowOne,
            color: theme.palette.text.primary,
            borderRadius: 2,
            border: theme.palette.border.borderOne,
            margin: "auto",
            display: "flex",
          }}
          size="small"
        >
          Back to Home
        </Button>
      </Card>
    </Layout>
  );
};
export default SuccessPage;
