/* eslint-disable no-param-reassign */
import { Container, Grid, Typography } from "@mui/material";
import React, { useContext } from "react";
import { useLocation, useNavigate } from "react-router-dom";
import Layout from "../../components/Layout";
import router from "../../config/router";
import { AlertContext } from "../../Providers/AlertProvider";
import { useTheme } from "../../Providers/ThemeProvider";
import { MakeRequest } from "../../utils";
import CheckoutForm from "../../components/checkoutForn";
import CartCard from "../../components/cartCard";

const CheckoutPage = ({ isPrivate }) => {
  const { setAlertInfo } = useContext(AlertContext);
  const navigate = useNavigate();
  const { theme } = useTheme();
  const { state } = useLocation();

  const handleCheckout = (data) => {
    data.serviceID = state.selectedServiceID;
    data.tierID = state._id;
    data.country = data.country.value;
    data.state = data.state.value;

    MakeRequest(router.Api.Signup, "POST", data).then((res) => {
      if (res.success) {
        navigate(router.SuccessPage.path, {
          state: {
            ...data,
            serviceName: state.name,
            tierName: state.selectedService,
          },
        });
      } else {
        setAlertInfo({
          msg: "Something went wrong",
          severity: "error",
        });
      }
    });
  };

  return (
    <Layout isCenter isPrivate={isPrivate}>
      <Typography
        variant="h4"
        sx={{
          paddingTop: 6,
          textAlign: "center",
        }}
      >
        Checkout form
      </Typography>
      <Typography
        variant="subtitle1"
        sx={{
          fontSize: 24,
          textAlign: "center",
          padding: 8,
        }}
      >
        {`You have Choosen the ${state.name} Tier for ${state.selectedService} Service. `}
      </Typography>
      <Container maxWidth="lg">
        <Grid container spacing={2}>
          <Grid item md={8} sm={12}>
            <CheckoutForm handleCheckoutForm={handleCheckout} />
          </Grid>
          <Grid item md={4} sm={12}>
            <CartCard cardDetails={state} />
          </Grid>
        </Grid>
      </Container>
    </Layout>
  );
};
export default CheckoutPage;
