export * as Home from "./Home";
export * as Login from "./Login";
export * as DetailPage from "./viewDetail";
export * as CheckoutPage from "./checkoutPage";
export * as SuccessPage from "./successPage";
