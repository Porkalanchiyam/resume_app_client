import { useNavigate } from "react-router-dom";
import Button from "../components/Button";
import Layout from "../components/Layout";
import { useTheme } from "../Providers/ThemeProvider";

const Login = ({
  isPrivate,
}) => {
  const navigate = useNavigate();
  return (
    <Layout isPrivate={isPrivate}>
      <Button
        onClick={() => {
          navigate("/");
        }}
      >
        Login Page
      </Button>
    </Layout>
  );
};

export default Login;
