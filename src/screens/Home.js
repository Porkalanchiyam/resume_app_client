import { useContext, useEffect, useState } from "react";
import { Container, Grid, Typography } from "@mui/material";
import Paper from "@mui/material/Paper";
import { styled } from "@mui/material/styles";
import Layout from "../components/Layout";
import MultiActionAreaCard from "../components/card";
import { MakeRequest } from "../utils";
import router from "../config/router";
import { AlertContext } from "../Providers/AlertProvider";

const Home = ({ isPrivate }) => {
  const { setAlertInfo } = useContext(AlertContext);
  const [services, setServices] = useState([]);

  useEffect(() => {
    MakeRequest(router.Api.GetService, "GET").then((res) => {
      if (res.success) {
        setServices(res.data);
      } else {
        setAlertInfo(res.data, "error");
      }
    });
  }, []);

  return (
    <Layout isPrivate={isPrivate}>
      <>
        <Typography
          variant="h4"
          sx={{
            paddingTop: 6,
            textAlign: "center",
          }}
        >
          Hi there,
        </Typography>
        <Typography
          variant="subtitle1"
          sx={{
            fontSize: 24,
            textAlign: "center",
            padding: 8,
          }}
        >
          We Offer you a wide range of services to help you get the best out of
          your career. To Get your dream job. Select any of the services below
          to get started.
        </Typography>

        <Container maxWidth="lg">
          <Grid
            container
            direction="row"
            justifyContent="center"
            alignItems="center"
            spacing={2}
          >
            {services?.map((service, index) => (
              <Grid item md={4} sm={10} xs={10} key={service._id}>
                <MultiActionAreaCard {...service} key={service._id} />
              </Grid>
            ))}
          </Grid>
        </Container>
      </>
    </Layout>
  );
};

export default Home;
