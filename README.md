# Getting Started

## Available Scripts

In the project directory, you can run:

For Development:

### `npm Install`

All the required Packages are installed.

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.

For Production:

### Deployment

### `npm Install`

All the required Packages are installed.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!\
Now Static HTML file will be generated. Serve the File using the Web Server.
